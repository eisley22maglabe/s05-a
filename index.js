// Mathematical Operations (-, *, /, %)
// Subtraction 
let numstring1 = "5";
let numstring2 = "6";
let num1 = 5;
let num2 = 6;
let num3 = 5.5;
let num4 = .5;

console.log(num1-num3);
console.log(num3-num4);
console.log(numstring1-num2);
console.log(numstring2-num2);
console.log(numstring1-numstring2);

let sample2 = "juan Dela Cruz";
console.log(sample2 - numstring2);

//multiply
console.log(num1*num2);
console.log(numstring1*num1);
console.log(numstring1*numstring2);
console.log(sample2*num2);

let product = num1 * num2;
let product2 = numstring1 * num1;
let product3 = numstring1 * numstring2;

console.log(product);

//division
console.log(product/num2);
console.log(product2/5);
console.log(numstring2/numstring1);

//divison/multiply by
console.log(product2 * 0);
console.log(product3 * 0);

//modulo
console.log(product2 % num2);
console.log(product3 % product2);
console.log(num1 % num2);
console.log(num1 % num1);

//boolean (true or false)
let isadmin = true;
let ismarried = false;
let isMVP = true;

console.log("Is she married" + ismarried);
console.log("Is he the MVP?" + isMVP);
console.log("Is he the current Admin" + isadmin);

//arrays
let array1 = ["goku","spongebob","lebron", "kpop"];
console.log(array1);

let array2 = ["john", true, 500, "jeff"];
console.log(array2);

let array3 = [98.7,92.1,90.2,94.6];
console.log(array3);

//objects
let person = {
	fullName: 'Lebron James',
	age: 35,
	ismarried: false,
	contact: ["09473534399", "54646456464"],
	address: {
		houseNumber: '345',
		street:'Diamond',
		city: 'manila'
	}
	};
console.log(person);

/* mini activity
	create a variable with a group of data
		-the group of data should contain names from your favorite ban

	create a vriable which contain multiple values of differing types adn describes a single person.
		- this data tyype shoulbe be able to contain multiple key values pairs;
			first name:
			lastname:
			hasportfolio:
			age:
			contct:
			address:
				housenumber:
				street:
				city:
*/
let twice = ["Mina","Dahyun", "Jihyo", "Sana", "Momo"];

let aboutme = {

		firstName: "Eisley Kirk",
		lastName: "Maglabe",
		isDeveloper: false,
		hasPortfolio: false,
		age: 21,
		contact: ["09473534399", "0947696969"],
		address: { 
			houseNumber: 420,
			street: 'Grove street',
			city: "Los Santos"

		}

};
console.log(twice);
console.log(aboutme);

//null vs undefine
let samplenull = null;
console.log(samplenull);

let sampleundefine;
console.log(sampleundefine);

let foundresult = null;
let mynum = 0;
let person2 = {
	name: "peter"
}

console.log(person2.isadmin);

//function

function printName(){
	console.log("My name is Jin");
};

printName();
printName();
printName();
printName();
printName();
printName();
printName();
printName();
printName();
printName();


function showsum(){
	console.log(25+6);
};

showsum();

function showsum(){
	console.log(69+6);
};

showsum();


// parameters and arguements
function printName(name){
	console.log(`My name is ${name}`);

};

printName("v");

function displaynum (number){
	alert(number);


};

displaynum(1000);

//Mini-Activity

function program(language){
	console.log(`${language} is fun`);
}
program("javascript");
program("C");

//multiple parameters
function displayfullname(firstname, mi, lastname, age){
	console.log (`${firstname} ${mi} ${lastname} ${age}`);
};

displayfullname("Kirk","P","jennifer", 32);

//return
function createfullname(firstname, mi, lastname, age){
	return `${firstname} ${mi} ${lastname} ${age}`
	console.log ("I will no longer rin because the function's value/result has been returned.");
};

let fullname1 = createfullname("Kirk","P","jennifer", 32);
let fullname2 = createfullname("Kirk","P","jennifer", 32);
let fullname3 = createfullname("Kirk","P","jennifer", 32);

console.log(fullname1);
console.log(fullname2);
console.log(fullname3);